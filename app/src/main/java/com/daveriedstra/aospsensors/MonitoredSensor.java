package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.TextView;

public class MonitoredSensor implements SensorEventListener {
    public Sensor sensor;
    public int sensorType;
    public TextView tvName;
    public int tvNameId;
    public TextView tvValue;
    public int tvValueId;

    public MonitoredSensor(int _sensorType, int _tvNameId, int _tvValueId) {
        sensorType = _sensorType;
        tvNameId = _tvNameId;
        tvValueId = _tvValueId;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onSensorChanged (SensorEvent e) { }
}
