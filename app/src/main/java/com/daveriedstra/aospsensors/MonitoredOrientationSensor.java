package com.daveriedstra.aospsensors;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.widget.TextView;

import org.puredata.core.PdBase;

public class MonitoredOrientationSensor extends MonitoredSensor {
    public static float azimuth;
    public static float pitch;
    public static float roll;

    public static TextView azimuthTv;
    public static TextView pitchTv;
    public static TextView rollTv;

    public MonitoredOrientationSensor(int _tvNameId, int _tvValueId) {
        super(-1, _tvNameId, _tvValueId);
    }


    @Override
    public void onSensorChanged (SensorEvent e) {
        throw new Error("Incorrect usage of orientation pseudo-sensor");
    }

    /**
     * Orientation update should be called after every
     * accelerometer, magnetoscope, and rotation event.
     *
     * This method updates the static orientation vector
     * members using the data from the above sensors. It
     * also sends the corresponding puredata messages.
     *
     * NB: calculated and displayed values in Radians;
     * values sent to pd in -1 to 1 range.
     */
    public static void update () {
        MonitoredRotationSensor.rotationMatrixOk = SensorManager.getRotationMatrix(
                MonitoredRotationSensor.rotationMatrix, null, MonitoredAccelSensor.data,
                MonitoredMagneticSensor.data);

        if (MonitoredRotationSensor.rotationMatrixOk) {
            float[] values = new float[3];
            SensorManager.getOrientation(MonitoredRotationSensor.rotationMatrix, values);
            azimuth = values[0];
            pitch = values[1];
            roll = values[2];

            if (MainActivity.UPDATE_TEXT) {
                azimuthTv.setText(String.format("%.2f", azimuth));
                pitchTv.setText(String.format("%.2f", pitch));
                rollTv.setText(String.format("%.2f", roll));
            }

            PdBase.sendFloat(PdResources.AzimuthSend, azimuth / (float)Math.PI);
            PdBase.sendFloat(PdResources.PitchSend, 2f * pitch / (float)Math.PI); // -180 - +180
            PdBase.sendFloat(PdResources.RollSend, roll / (float)Math.PI);
        }
    }
}
