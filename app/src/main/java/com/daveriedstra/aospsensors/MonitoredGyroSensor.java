package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import org.puredata.core.PdBase;

public class MonitoredGyroSensor extends MonitoredSensor {
    public MonitoredGyroSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_GYROSCOPE, _tvNameId, _tvValueId);
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        if (MainActivity.UPDATE_TEXT) {
            tvValue.setText(String.format("X: %.2f, Y: %.2f, Z: %.2f rad / s",
                    e.values[0], e.values[1], e.values[2]));
        }

        PdBase.sendFloat(PdResources.GyroXSend, e.values[0] / (float)Math.PI);
        PdBase.sendFloat(PdResources.GyroYSend, e.values[1] / (float)Math.PI);
        PdBase.sendFloat(PdResources.GyroZSend, e.values[2] / (float)Math.PI);
    }
}
