package com.daveriedstra.aospsensors;

abstract class PdResources {
    public static final String AzimuthSend = "a-azimuth";
    public static final String PitchSend = "a-pitch";
    public static final String RollSend = "a-roll";

    public static final String GyroXSend = "a-gyro-x";
    public static final String GyroYSend = "a-gyro-y";
    public static final String GyroZSend = "a-gyro-z";

    public static final String ProxSend = "a-prox";
    public static final String BatteryTempSend = "a-batt-temp";

    public static final String LuxDeviationSend = "a-lux-dev";
    public static final String MagneticIntensityDeviationSend = "a-magnetic-dev";

    public static final String MagneticXSend = "a-magnetic-y";
    public static final String MagneticYSend = "a-magnetic-x";
    public static final String MagneticZSend = "a-magnetic-z";
}
