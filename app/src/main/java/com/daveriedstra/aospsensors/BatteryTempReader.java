package com.daveriedstra.aospsensors;

import android.os.Handler;
import android.widget.TextView;

import org.puredata.core.PdBase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BatteryTempReader {
    private static final String BATT_TEMP_FILE =
            "/sys/devices/platform/i2c-gpio.6/i2c-6/6-0066/max8998-charger/power_supply/battery/temp";
    private float lastTemp = 0f;
    public TextView batteryTv;

    public int interval = 1000; // ms
    private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateTemp();
            handler.postDelayed(runnable, interval);
        }
    };

    public void start() {
        handler.postDelayed(runnable, interval);
    }

    public void stop() {
        handler.removeCallbacks(runnable);
    }

    private void updateTemp() {
        float newTemp = getBatteryTemp();
        if (lastTemp == newTemp)
            return;

        lastTemp = newTemp;
        if (MainActivity.UPDATE_TEXT)
            batteryTv.setText(String.format("%.2f deg c", newTemp));
        PdBase.sendFloat(PdResources.BatteryTempSend, newTemp);
    }

    public float getBatteryTemp() {
        float temp = 0;
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(BATT_TEMP_FILE));
            String line;
            while ((line = reader.readLine()) != null) {
                try {
                    temp = Float.valueOf(line) / 10f;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return temp;
    }
}
