package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class MonitoredLuxSensor extends AveragingMonitoredSensor {
    public MonitoredLuxSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_LIGHT, _tvNameId, _tvValueId);
        pdDeviationAddress = PdResources.LuxDeviationSend;
        units = "lx";
    }

    protected float getValue(SensorEvent e) {
        return e.values[0];
    }
}
