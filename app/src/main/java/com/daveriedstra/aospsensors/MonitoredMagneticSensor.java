package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.widget.TextView;

import org.puredata.core.PdBase;

public class MonitoredMagneticSensor extends AveragingMonitoredSensor {
    // the data from the last sensor change, used in orientation calculations
    public static float[] data = new float[3];
    public TextView directionTv;

    public MonitoredMagneticSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_MAGNETIC_FIELD, _tvNameId, _tvValueId);
        pdDeviationAddress = PdResources.MagneticIntensityDeviationSend;
        units = "muT";
        ACCEPTABLE_DEVIATION = 5f;
    }

    // magnetic field: XYZ in muT
    @Override
    public void onSensorChanged (SensorEvent e) {
        // first set static data for orientation calculation
        data = e.values.clone();
        MonitoredOrientationSensor.update();

        // now continue on with our own business
        super.onSensorChanged(e);

        if (MainActivity.UPDATE_TEXT)
            directionTv.setText(String.format("X: %.2f, Y: %.2f, Z: %.2f", data[0], data[1], data[2]));

        // send the vector values, but scale to 1
        // this allows us to use them as relative intensities
        // rather than absolute values.

        float max = -9999;

        for (float val : data) {
            val = Math.abs(val);
            if (val > max)
                max = val;
        }

        // prevent divide by 0
        if (max == 0f)
            max = 0.0000001f;
        float sendX = Math.abs(data[0] / max);
        float sendY = Math.abs(data[1] / max);
        float sendZ = Math.abs(data[2] / max);

        PdBase.sendFloat(PdResources.MagneticXSend, sendX);
        PdBase.sendFloat(PdResources.MagneticYSend, sendY);
        PdBase.sendFloat(PdResources.MagneticZSend, sendZ);
    }

    /**
     * For our own concerns, the value is the average magnetic intensity in all 3 directions
     */
    protected float getValue(SensorEvent e) {
        float x = e.values[0];
        float y = e.values[1];
        float z = e.values[2];

        return (float) Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
    }
}
