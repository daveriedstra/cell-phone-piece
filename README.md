# AOSP Sensors
__sound sculpture for Nexus S (crespo)__

AOSP Sensors generates sound by attuning to its surroundings. Information about ambient light, position, temperature, and other aspects of the device’s situation is monitored in real-time. The piece uses this data to manipulate sound sampled from the environment. In this way, AOSP Sensors demonstrates faculty for aesthetic perception, entrainment/entertainment of the senses, and even self-awareness.

[video](https://www.youtube.com/watch?v=pX8uRk_z6No)

The android app simply sends sensor data to the puredata patch, which uses the data to manipulate microphone input in realtime. The mic input is played back at a slower sample rate to expose some complexities in the processing (and to reduce feedback).

The app is purpose-built for Nexus S and is unlikely to work fully-featured on other devices – but you're welcome to try.